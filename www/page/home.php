<div class="home box">
    <h1>Vitajte na stránke PC Guru</h1>
    <p>Spoločnosť PC Guru je vzdelávacia spoločnosť,<br> ktorá Vám sprostredkuje oceán vedomostí.</p>
    <p>
        <a class="btn btn-lg btn-primary" href="/kurzy" role="button">Zoznam aktuálnych kurzov</a>
    </p>
</div>
