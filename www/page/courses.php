<div class="box">
<?php

la($url, 'url');
la($_POST, '_POST');
$server = 'https://pcguru.eduoffice.sk';
if ($_SERVER['SERVER_ADDR'] === '127.0.0.1') {
    $server = 'http://localhost:8000';
}


# create widget url
$widgetUrl = '/public/term';
if ($url['path'] === '/kurzy2') {
    $widgetUrl = '/public/term/compact';
}
if (isset($url['query'])) {
    parse_str($url['query'], $params);
    la($params, 'url params');
    if (isset($params['category'])) {
        $widgetUrl .= "?category=$params[category]";
    } elseif (isset($params['term'])) {
        $widgetUrl .= "/$params[term]";
    } elseif (isset($params['join'])) {
        $widgetUrl = "/public/term-join/$params[join]";
    }
}

# if join form was filled
$context = null;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($_POST)
        )
    );
    $context  = stream_context_create($options);
}

# request widget content and handle errors
set_error_handler(
    create_function(
        '$severity, $message, $file, $line',
        'throw new ErrorException($message, $severity, $severity, $file, $line);'
    )
);
$content = null;
try {
    $content = file_get_contents($server . $widgetUrl, false, $context);
} catch (Exception $e) {
    $error = $e->getMessage();
}
restore_error_handler();

if ($content) {
    # select only <div class="eduoffice">
    $eduoffice_start = strpos($content, '<div class="eduoffice">');
    $eduoffice = substr($content, $eduoffice_start);
    $end = '<!-- eduoffice-end-->';
    $eduoffice_end = strpos($eduoffice, $end);
    $eduoffice = substr($eduoffice, 0, $eduoffice_end + strlen($end));

    # fix widget urls
    $eduoffice = str_replace('"/public/term"', '"/kurzy"', $eduoffice);
    $eduoffice = str_replace('/public/term/', '/kurzy?term=', $eduoffice);
    $eduoffice = str_replace('/public/term-join/', '/kurzy?join=', $eduoffice);
    echo $eduoffice . "\n";
} elseif (strpos($error, '440')) {
    include 'errorDuplicity.php';
} else {
    include 'error.php';
}
?>
</div>