<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PC Guru</title>
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css?v=1">
    <link rel="stylesheet" href="css/style.css?v=1">
    <link rel="stylesheet" href="https://pcguru.eduoffice.sk/styles/widget.css?v=1">
</head>
<body>
<?php
    include __DIR__ . '/common.php';
    $url = parse_url('/');
    if (array_key_exists('q', $_GET)) {
        $url = parse_url($_GET['q']);
    }
?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <span class="logo-pc">PC</span> <span class="logo-guru">Guru</span>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><?= menu($url, '/', 'Domov') ?></li>
                <li><?= menu($url, '/kurzy', 'Aktuálne kurzy') ?></li>
                <li><?= menu($url, '/kurzy2', 'Aktuálne kurzy 2') ?></li>
                <li><?= menu($url, '/kontakt', 'Kontakt') ?></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">
<?php
    if ($url['path'] === '/kontakt') {
        include __DIR__ . '/page/contact.php';
    } elseif ($url['path'] === '/kurzy') {
        include __DIR__ . '/page/courses.php';
    } elseif ($url['path'] === '/kurzy2') {
        include __DIR__ . '/page/courses.php';
    } else {
        include __DIR__ . '/page/home.php';
    };
?>
</div> <!-- /container -->

</body>
</html>