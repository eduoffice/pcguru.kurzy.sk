# Implementácia eduoffice widget do vlastnej web stránky #

Tento projekt slúži ako ukážková implementácia eduoffice widgetu do web stránky v php.

Táto stránka je jednoduchá stránka vymyslenej firmy nachádza sa na stránke http://pcguru.kurzy.sk/

Na podstránke http://pcguru.kurzy.sk/kurzy alebo http://pcguru.kurzy.sk/kurzy2 je možné pozrieť si ako vyzerá eduoffice widget, keď je vložený do stránky.

Ukážka implementácie widgetu v php je možné si pozrieť v tomto súbore [www/page/courses.php](https://bitbucket.org/eduoffice/pcguru.kurzy.sk/src/49ffd4af337b5324f1ecae77f2c42e3b150735af/www/page/courses.php?at=master&fileviewer=file-view-default)
